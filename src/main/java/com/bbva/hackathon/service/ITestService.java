package com.bbva.hackathon.service;

import com.bbva.hackathon.domain.Test;

public interface ITestService {

	Test getTest();
}
