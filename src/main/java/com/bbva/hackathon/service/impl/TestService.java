package com.bbva.hackathon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.stereotype.Service;

import com.bbva.hackathon.domain.Test;
import com.bbva.hackathon.repository.TestRepository;
import com.bbva.hackathon.service.ITestService;

@Service
public class TestService implements ITestService{

	@Autowired
	private TestRepository testRepository;
	
	@Override
	@ReadOnlyProperty
	public Test getTest() {
		
		return testRepository.getOne(1);
	}	
}
