package com.bbva.hackathon.repository.jpa.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.bbva.hackathon.domain.Test;
import com.bbva.hackathon.repository.jpa.TestRepositoryJpa;

public class TestRepositoryJpaImpl extends RepositoryAdapter<Test> implements TestRepositoryJpa{

	@Override
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager){
		super.setEntityManager(entityManager);
	}
}
