package com.bbva.hackathon.repository;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bbva.hackathon.domain.Test;
import com.bbva.hackathon.repository.jpa.TestRepositoryJpa;

@Valid
public interface TestRepository extends JpaRepository<Test, Integer>, TestRepositoryJpa{

	
}
