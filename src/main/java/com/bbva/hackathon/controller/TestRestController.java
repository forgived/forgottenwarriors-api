package com.bbva.hackathon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.hackathon.domain.Test;
import com.bbva.hackathon.service.ITestService;
import com.bbva.hackathon.service.impl.TestService;

@RestController
public class TestRestController {

	@Autowired
	private ITestService testService;
	
	@RequestMapping("/test")
	public Integer test(){
		return testService.getTest().getId();
	}
	
}
